PI = 3.14159


def print_area_of_sphere(radius):
    surface_area = 4 * PI * radius ** 2
    print("Surface area for radius =", radius, "is", surface_area)


def print_volume_of_sphere(radius):
    volume = 4 / 3 * PI * radius ** 3
    print("Volume for radius =", radius, "is", volume)


print_area_of_sphere(0.7583)  # Expect 7.2259
print_area_of_sphere(0.0955)  # Expect 0.11461
print_area_of_sphere(0.4792)  # Expect 2.88565
print_volume_of_sphere(0.4154)  # Expect 0.30025
print_volume_of_sphere(0.9566)  # Expect 3.66674
print_volume_of_sphere(0.9609)  # Expect 3.71641
