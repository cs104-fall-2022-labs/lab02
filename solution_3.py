

cs101_credits = 6
eng101_credits = 4
math103_credits = 6
phys101_credits = 8
tll101_credits = 4

total_credits = cs101_credits + eng101_credits + math103_credits + phys101_credits + tll101_credits

A = 4.
Aminus = 3.7
Bplus = 3.3
B = 3.
Bminus = 2.7
Cplus = 2.3
C = 2.
Cminus = 1.7
Dplus = 1.3
D = 1.
F = 0.

merve_cs101_score = A
merve_eng101_score = Bminus
merve_math103_score = Cplus
merve_phys101_score = Dplus
merve_tll101_score = Aminus

ozlem_cs101_score = Bplus
ozlem_eng101_score = C
ozlem_math103_score = Aminus
ozlem_phys101_score = D
ozlem_tll101_score = A


def calculate_gpa(cs101_score, eng101_score, math103_score, phys101_score, tll101_score):
    return (cs101_score * cs101_credits + eng101_score * eng101_credits + math103_score * math103_credits + phys101_score * phys101_credits + tll101_score * tll101_credits) / total_credits


merve_gpa = calculate_gpa(merve_cs101_score, merve_eng101_score, merve_math103_score, merve_phys101_score, merve_tll101_score)
ozlem_gpa = calculate_gpa(ozlem_cs101_score, ozlem_eng101_score, ozlem_math103_score, ozlem_phys101_score, ozlem_tll101_score)

print("Ozlem\'s GPA is ", ozlem_gpa)
print("Merve\'s GPA is ", merve_gpa)
