def sequence(n):
    print((2 * n + 1) / 2 ** (n - 1))


# Examples
sequence(2)  # Expect 2.5
sequence(4)  # Expect 1.125

# Demonstrate
sequence(5)
sequence(8)
sequence(11)
